package com.nkming.nc_photos.plugin

interface K {
	companion object {
		const val DOWNLOAD_NOTIFICATION_ID_MIN = 1000
		const val DOWNLOAD_NOTIFICATION_ID_MAX = 2000

		const val LIB_ID = "com.nkming.nc_photos.plugin"

		const val ACTION_DOWNLOAD_CANCEL = "com.nkming.nc_photos.ACTION_DOWNLOAD_CANCEL"

		const val EXTRA_NOTIFICATION_ID = "com.nkming.nc_photos.EXTRA_NOTIFICATION_ID"
	}
}
