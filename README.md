# Photos
Browse your photos on Nextcloud servers

<script src="https://liberapay.com/nkming2/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/nkming2/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>

## Get the app
### Android
[<img src="https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png" alt="Google Play" width="160" />](https://play.google.com/store/apps/details?id=com.nkming.nc_photos.paid&referrer=utm_source%3Drepo)  
Or the [ad-supported version](https://play.google.com/store/apps/details?id=com.nkming.nc_photos&referrer=utm_source%3Drepo)

### Web (experimental)
Please read [this guide](https://gitlab.com/nkming2/nc-photos/-/wikis/help/web-app)

### Compile yourself
Please read [this guide](https://gitlab.com/nkming2/nc-photos/-/wikis/development/build)

## Features
- Support JPEG, PNG, WebP, HEIC, GIF images
- Support MP4, WebM videos (codec support may vary between devices/browsers)
- EXIF support (JPEG and HEIC only)
- Organize photos with albums that are independent of your file hierarchy
- Sign-in to multiple servers
- Create shared albums with users on the same server (experimental)

## Supported Nextcloud apps
- Face Recognition (require 0.8.5+)
  - Please read [this guide](https://gitlab.com/nkming2/nc-photos/-/wikis/help/people)

## Translations (sorted by ISO name)
- Czech/čeština (contributed by Skyhawk)
- Finnish/suomi (contributed by pHamala)
- French/français (contributed by mgreil)
- German/Deutsch (contributed by PhilProg)
- Greek/ελληνικά (contributed by Chris Karasoulis)
- Polish/język polski (contributed by szymok)
- Russian/русский (contributed by meixnt & eriane)
- Spanish/Español (contributed by luckkmaxx)

## Contributions, troubleshooting and guides
Please visit the [project wiki](https://gitlab.com/nkming2/nc-photos/-/wikis/home)
